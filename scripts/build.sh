#!/bin/bash

apk update && apk add openjdk8 maven git gettext
mkdir docker-content
cp -r configs/$CI_ENVIRONMENT_SLUG/* docker-content
cp -r email_templates docker-content
for f in $(find docker-content -type f); do
    value=$(envsubst < "$f")
    echo -e "$value" > $f
    echo "$f:"
    cat $f
done
git clone https://github.com/AlexMog/ApiLib && cd ApiLib && mvn -q install && cd -
mvn -q compile assembly:single
cp target/*.jar docker-content/exec.jar
docker build -t localbuild .
