FROM openjdk:8-slim-buster

WORKDIR /ffs

COPY docker-content/ ./

#RUN for f in $(find . -name "*.json"); do envsubst < $f > $f; done
#RUN for f in $(find . -name "*.properties"); do envsubst < $f > $f; done

CMD java -jar exec.jar
